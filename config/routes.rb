# frozen_string_literal: true

Rails.application.routes.draw do
  scope defaults: { format: 'json' } do
    namespace :api, path: '/' do
      scope module: :v1 do
        # Concerns.
        concern :purchasable do
          resources :purchases, only: %i[index create]
        end

        get 'products', to: 'products#index'
        resources :seasons, only: :index
        resources :movies, only: :index
        resources :users, only: [], concerns: %i[purchasable]
      end
    end
  end
end
