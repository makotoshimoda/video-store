# frozen_string_literal: true

configuration = {
  url: "redis://#{ENV['REDIS_HOST']}:#{ENV['REDIS_PORT']}",
  namespace: "#{Rails.env}_video_store_sidekiq"
}
configuration[:password] = ENV['REDIS_PASS'] if ENV['REDIS_PASS'].present?

Sidekiq.configure_server do |config|
  config.redis = configuration
end

Sidekiq.configure_client do |config|
  config.redis = configuration
end
