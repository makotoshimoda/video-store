class CreateOptions < ActiveRecord::Migration[5.2]
  def change
    create_table :options do |t|
      t.money :price, default: 0
      t.string :quality
      t.references :optionable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
