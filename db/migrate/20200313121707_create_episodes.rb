class CreateEpisodes < ActiveRecord::Migration[5.2]
  def change
    create_table :episodes do |t|
      t.string :title
      t.text :plot
      t.integer :number
      t.integer :season_id

      t.timestamps
    end

    add_index :episodes, :season_id
  end
end
