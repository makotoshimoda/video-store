class CreatePurchases < ActiveRecord::Migration[5.2]
  def change
    create_table :purchases do |t|
      t.integer :option_id
      t.integer :user_id
      t.references :purchasable, polymorphic: true, index: true

      t.timestamps
      t.datetime :deleted_at
    end

    add_index :purchases, :option_id
    add_index :purchases, :user_id
  end
end
