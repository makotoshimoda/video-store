SELECT
  'Movie' as model,
  id,
  title,
  plot,
  created_at,
  updated_at
FROM
  movies

UNION

SELECT
  'Season' as model,
  id,
  title,
  plot,
  created_at,
  updated_at
FROM
  seasons

ORDER BY
  created_at
