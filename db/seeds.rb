# frozen_string_literal: true

movies = Movie.create([{ title: 'Star Wars', plot: 'Star Wars' },
                       { title: 'Lord of the Rings', plot: 'Lord of the Rings' },
                       { title: 'Toy Story', plot: 'Toy Story' }])

seasons = Season.create([{ title: 'Star Wars season', plot: 'Star Wars season' },
                         { title: 'Lord of the Rings season', plot: 'Lord of the Rings season' }])

users = User.create([{ email: 'user1@test.com' }, { email: 'user2@test.com' }])

movies.each do |movie|
  %w[HD SD].each do |quality|
    Option.create(price: 2.99, quality: quality, optionable: movie)
  end
end

seasons.each do |season|
  %w[HD SD].each do |quality|
    Option.create(price: 2.99, quality: quality, optionable: season)
  end
end

Purchase.create([{ user: users.first, purchasable: movies.first, option: movies.first.options.first },
                 { user: users.first, purchasable: seasons.last, option: seasons.last.options.first },
                 { user: users.second, purchasable: seasons.first, option: seasons.first.options.last }])

Episode.create([{ title: 'Star Wars ep 1', plot: 'Star Wars ep 1', number: 1, season: seasons.first },
                { title: 'Star Wars ep 2', plot: 'Star Wars ep 2', number: 2, season: seasons.first },
                { title: 'Lord of the Rings ep 2', plot: 'Lord of the Rings ep 2', number: 2, season: seasons.last },
                { title: 'Lord of the Rings ep 1', plot: 'Lord of the Rings ep 1', number: 1, season: seasons.last }])
