# frozen_string_literal: true

class Purchase < ApplicationRecord
  include SoftDeletable

  # Callbacks.
  after_save :set_purchase_expiration

  # Associations.
  belongs_to :purchasable, polymorphic: true
  belongs_to :option
  belongs_to :user

  # Validations.
  validates_uniqueness_of :purchasable_id, scope: %i[user_id purchasable_type], conditions: -> { where(deleted_at: nil) }

  protected

  # Trigger an action when a purchase is made.
  #
  # ==== Returns
  # * <tt>void</tt>
  def set_purchase_expiration
    PurchaseEndWorker.perform_at(Time.now + 2.days, id)
  end
end
