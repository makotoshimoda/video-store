# frozen_string_literal: true

class Option < ApplicationRecord
  # Constants.
  OPTION_QUALITIES = %w[SD HD].freeze

  # Associations.
  belongs_to :optionable, polymorphic: true

  # Validations.
  validates :price, numericality: true
  validates :quality, inclusion: { in: OPTION_QUALITIES }
end
