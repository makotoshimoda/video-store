# frozen_string_literal: true

class Episode < ApplicationRecord
  # Associations.
  belongs_to :season

  # Validations.
  validates :title, :plot, :number, presence: true
  validates :number, numericality: true
end
