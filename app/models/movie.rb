# frozen_string_literal: true

class Movie < ApplicationRecord
  include Optionable
  include Purchasable

  # Validations.
  validates :title, :plot, presence: true
end
