# frozen_string_literal: true

class Season < ApplicationRecord
  include Optionable
  include Purchasable

  # Associations.
  has_many :episodes

  # Validations.
  validates :title, :plot, presence: true
end
