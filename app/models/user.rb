# frozen_string_literal: true

class User < ApplicationRecord
  # Associations.
  has_many :purchases

  # Validations.
  validates :email, presence: true, 'valid_email_2/email': true
end
