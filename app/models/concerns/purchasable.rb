# frozen_string_literal: true

module Purchasable
  extend ActiveSupport::Concern

  included do
    has_many :purchases, as: :purchasable
  end
end
