# frozen_string_literal: true

module SoftDeletable
  extend ActiveSupport::Concern

  included do
    default_scope -> { where(deleted_at: nil) }
  end

  # Soft delete a record.
  #
  # ==== Returns
  # * <tt>ActiveRecord::Model</tt> - Instance.
  def soft_delete
    update(deleted_at: Time.zone.now)
  end
end
