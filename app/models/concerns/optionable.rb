# frozen_string_literal: true

module Optionable
  extend ActiveSupport::Concern

  included do
    has_many :options, as: :optionable
  end
end
