# frozen_string_literal: true

class PurchaseEndWorker
  include Sidekiq::Worker

  # Soft delete a purchase expire.
  #
  # ==== Parameters
  # * <tt>purchase_id</tt> - Purchase id.
  #
  # ==== Returns
  # * <tt>void</tt>.
  def perform(purchase_id)
    purchase = Purchase.find_by(id: purchase_id)
    purchase&.soft_delete
  end
end
