# frozen_string_literal: true

module Api
  module V1
    class ProductsController < Api::ApiController
      pagination_headers_for :products, only: %i[index]

      # Get products.
      #
      # ==== Returns
      # * <tt>Response</tt> - JSON serialized products.
      def index
        @products = Product.all.page(params[:page]).per(params[:per])

        render json: @products, each_serializer: ProductSerializer, status: :ok, location: api_products_url
      end
    end
  end
end
