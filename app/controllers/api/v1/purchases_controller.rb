# frozen_string_literal: true

module Api
  module V1
    class PurchasesController < Api::ApiController
      pagination_headers_for :purchases, only: %i[index]

      # Get purchases.
      #
      # ==== Returns
      # * <tt>Response</tt> - JSON serialized seasons.
      def index
        return unless user

        @purchases = user.purchases.includes(:option, :purchasable).order(:created_at).page(params[:page]).per(params[:per])

        render json: @purchases, status: :ok, location: api_user_purchases_url
      end

      # Create a purchase.
      #
      # ==== Returns
      # * <tt>Response</tt> - JSON serialized purchase, errors otherwise.
      def create
        return unless user

        @purchase = user.purchases.new(purchase_params)

        if @purchase.save
          render json: @purchase, status: :created
        else
          render json: @purchase.errors, status: :unprocessable_entity
        end
      end

      private

      # Get user.
      #
      # ==== Returns
      # * <tt>ActiveRecord</tt> - User object.
      def user
        found_user = User.find_by(id: params[:user_id])

        render json: { error: "User with id #{params[:user_id]} not found." }, status: :unprocessable_entity unless found_user

        found_user
      end

      # Only allow a trusted parameter "white list" through.
      def purchase_params
        params.require(:purchase).permit(:purchasable_id, :purchasable_type, :option_id, :user_id)
      end
    end
  end
end
