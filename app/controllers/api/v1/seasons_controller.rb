# frozen_string_literal: true

module Api
  module V1
    class SeasonsController < Api::ApiController
      pagination_headers_for :seasons, only: %i[index]

      # Get seasons.
      #
      # ==== Returns
      # * <tt>Response</tt> - JSON serialized seasons.
      def index
        @seasons = Season.includes(:options).order(:created_at).page(params[:page]).per(params[:per])

        render json: @seasons, status: :ok, location: api_seasons_url
      end
    end
  end
end
