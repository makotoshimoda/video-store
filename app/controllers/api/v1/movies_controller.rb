# frozen_string_literal: true

module Api
  module V1
    class MoviesController < Api::ApiController
      pagination_headers_for :movies, only: %i[index]

      # Get movies.
      #
      # ==== Returns
      # * <tt>Response</tt> - JSON serialized movies.
      def index
        @movies = Movie.includes(:options).order(:created_at).page(params[:page]).per(params[:per])

        render json: @movies, status: :ok, location: api_movies_url
      end
    end
  end
end
