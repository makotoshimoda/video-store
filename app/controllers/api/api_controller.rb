# frozen_string_literal: true

module Api
  class ApiController < ApplicationController
    # rubocop:disable Metrics/AbcSize
    def self.pagination_headers_for(name, options = {})
      after_action(options) do |_controller|
        scope = instance_variable_get("@#{name}")

        if scope
          headers['X-Pagination-Limit']        = (params[:per] || 25).to_s
          headers['X-Pagination-Current-Page'] = scope.current_page.to_s
          headers['X-Pagination-Total-Pages']  = scope.total_pages.to_s
          headers['X-Pagination-Total-Count']  = scope.total_count.to_s
        end
      end
    end
    # rubocop:enable Metrics/AbcSize
  end
end
