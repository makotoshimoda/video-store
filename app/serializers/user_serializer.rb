# frozen_string_literal: true

# :nocov:
class UserSerializer < ActiveModel::Serializer
  attributes :id, :email
end
# :nocov:
