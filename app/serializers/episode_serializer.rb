# frozen_string_literal: true

# :nocov:
class EpisodeSerializer < ActiveModel::Serializer
  attributes :id, :title, :plot, :number, :season_id, :created_at, :updated_at
end
# :nocov:
