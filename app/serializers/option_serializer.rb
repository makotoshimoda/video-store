# frozen_string_literal: true

class OptionSerializer < ActiveModel::Serializer
  attributes :id, :price, :quality

  belongs_to :optionable
end
