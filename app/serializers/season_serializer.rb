# frozen_string_literal: true

class SeasonSerializer < ActiveModel::Serializer
  attributes :id, :title, :plot, :created_at, :updated_at

  has_many :options
  has_many :episodes do
    object.episodes.order(:number)
  end
end
