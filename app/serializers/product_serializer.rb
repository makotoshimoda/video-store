# frozen_string_literal: true

class ProductSerializer < ActiveModel::Serializer
  attributes :model, :product

  def product
    found = object.model.constantize.includes(:options).find_by(id: object.id)
    "#{object.model}Serializer".constantize.new(found).as_json
  end
end
