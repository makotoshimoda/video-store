# frozen_string_literal: true

class MovieSerializer < ActiveModel::Serializer
  attributes :id, :title, :plot, :created_at, :updated_at

  has_many :options
end
