# frozen_string_literal: true

class PurchaseSerializer < ActiveModel::Serializer
  attributes :id, :purchasable_type, :created_at, :updated_at

  belongs_to :purchasable
  belongs_to :option
end
