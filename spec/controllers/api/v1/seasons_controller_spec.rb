# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::SeasonsController, type: :controller do
  describe 'GET index' do
    before(:each) do
      10.times do
        FactoryBot.create(:season)
      end
    end

    it 'retrieves seasons' do
      get :index, params: { format: :json }
      expect(JSON.parse(response.body).length).to eq 10
    end

    it 'has a 200 ok status' do
      get :index, params: { format: :json }
      expect(response.status).to eq 200
    end

    it 'has an url location' do
      get :index, params: { format: :json }
      expect(response.location).to eq api_seasons_url
    end

    it 'contains pagination headers' do
      get :index, params: { format: :json, page: 2, per: 2 }

      expect(response.headers['X-Pagination-Limit']).to eq '2'
      expect(response.headers['X-Pagination-Current-Page']).to eq '2'
      expect(response.headers['X-Pagination-Total-Pages']).to eq '5'
      expect(response.headers['X-Pagination-Total-Count']).to eq '10'
    end
  end
end
