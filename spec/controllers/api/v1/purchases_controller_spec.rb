# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::PurchasesController, type: :controller do
  let(:user) { FactoryBot.create(:user) }
  let(:movie) { FactoryBot.create(:movie) }
  let(:option) { FactoryBot.create(:option, optionable: movie) }
  let(:attributes) { FactoryBot.build(:purchase, user: user, purchasable: movie, option: option).attributes }

  before(:each) do
    allow(User).to receive(:find_by).and_return(user)
  end

  describe 'GET index' do
    before(:each) do
      10.times do
        FactoryBot.create(:purchase, user: user)
      end
    end

    describe 'with existing user' do
      it 'retrieves purchases' do
        get :index, params: { format: :json, user_id: user }
        expect(JSON.parse(response.body).length).to eq 10
      end

      it 'has a 200 ok status' do
        get :index, params: { format: :json, user_id: user }
        expect(response.status).to eq 200
      end

      it 'has an url location' do
        get :index, params: { format: :json, user_id: user }
        expect(response.location).to eq api_user_purchases_url
      end

      it 'contains pagination headers' do
        get :index, params: { format: :json, user_id: user, page: 2, per: 2 }

        expect(response.headers['X-Pagination-Limit']).to eq '2'
        expect(response.headers['X-Pagination-Current-Page']).to eq '2'
        expect(response.headers['X-Pagination-Total-Pages']).to eq '5'
        expect(response.headers['X-Pagination-Total-Count']).to eq '10'
      end
    end

    describe 'with non-existing user' do
      before(:each) do
        allow(User).to receive(:find_by).and_return(nil)
      end

      it 'has a 422 unprocessable_entity status' do
        get :index, params: { format: :json, user_id: 10_000 }
        expect(response.status).to eq 422
      end
    end
  end

  describe 'POST create' do
    describe 'with existing user' do
      describe 'save successful' do
        it 'creates a new purchase' do
          expect do
            post :create, params: { purchase: attributes, format: :json, user_id: user }
          end.to change(user.purchases, :count).by(1)
        end

        it 'has a 201 status' do
          post :create, params: { purchase: attributes, format: :json, user_id: user }
          expect(response.status).to eq 201
        end
      end

      describe 'save failed' do
        it 'has a 422 unprocessable entity status' do
          allow_any_instance_of(Purchase).to receive(:save).and_return(false)
          allow_any_instance_of(Purchase).to receive(:errors)

          post :create, params: { purchase: attributes, format: :json, user_id: user }
          expect(response.status).to eq 422
        end
      end
    end

    describe 'with non-existing user' do
      before(:each) do
        allow(User).to receive(:find_by).and_return(nil)
      end

      it 'has a 422 unprocessable_entity status' do
        post :create, params: { purchase: attributes, format: :json, user_id: 10_000 }
        expect(response.status).to eq 422
      end
    end
  end
end
