# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PurchaseEndWorker do
  let(:purchase) { FactoryBot.build(:purchase) }

  before(:each) do
    allow(Purchase).to receive(:find_by).and_return(purchase)
    allow_any_instance_of(Purchase).to receive(:soft_delete)
  end

  it 'creates a new job in the queue' do
    expect do
      PurchaseEndWorker.perform_async(purchase.id)
    end.to change(PurchaseEndWorker.jobs, :size).by(1)
  end

  describe '#perform' do
    it 'finds the purchase pass as argument' do
      expect(Purchase).to receive(:find_by)
      PurchaseEndWorker.new.perform(purchase.id)
    end

    it 'soft delete the purchase' do
      expect_any_instance_of(Purchase).to receive(:soft_delete)
      PurchaseEndWorker.new.perform(purchase.id)
    end
  end
end
