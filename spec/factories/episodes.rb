# frozen_string_literal: true

FactoryBot.define do
  factory :episode do
    title { Faker::Lorem.characters(number: 20) }
    plot { Faker::Lorem.characters(number: 2000) }
    number { Faker::Number.number }

    association :season, factory: :season
  end
end
