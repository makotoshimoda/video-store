# frozen_string_literal: true

FactoryBot.define do
  factory :option do
    price { 2.99 }
    quality { 'HD' }

    association :optionable, factory: :movie
  end
end
