# frozen_string_literal: true

FactoryBot.define do
  factory :season do
    title { Faker::Lorem.characters(number: 20) }
    plot { Faker::Lorem.characters(number: 2000) }
  end
end
