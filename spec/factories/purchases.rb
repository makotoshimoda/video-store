# frozen_string_literal: true

FactoryBot.define do
  factory :purchase do
    association :purchasable, factory: :movie
    association :option, factory: :option
    association :user, factory: :user
  end
end
