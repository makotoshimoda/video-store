# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Episode, type: :model do
  let(:episode) { FactoryBot.build(:episode) }

  describe 'data validations' do
    describe '.title' do
      it 'should be defined' do
        episode.title = Faker::Lorem.characters(number: 10)
        expect(episode.valid?).to be_truthy
        expect(episode.errors[:title].size).to eq(0)

        episode.title = nil
        expect(episode.valid?).to be_falsey
        expect(episode.errors[:title].size).to eq(1)
      end
    end

    describe '.plot' do
      it 'should be defined' do
        episode.plot = Faker::Lorem.characters(number: 10)
        expect(episode.valid?).to be_truthy
        expect(episode.errors[:plot].size).to eq(0)

        episode.plot = nil
        expect(episode.valid?).to be_falsey
        expect(episode.errors[:plot].size).to eq(1)
      end
    end

    describe '.number' do
      it 'should be defined' do
        episode.number = Faker::Number.number(digits: 5)
        expect(episode.valid?).to be_truthy
        expect(episode.errors[:number].size).to eq(0)

        episode.number = nil
        expect(episode.valid?).to be_falsey
        expect(episode.errors[:number].size).to eq(2)
      end

      it 'should be a number' do
        episode.number = 10
        expect(episode.valid?).to be_truthy
        expect(episode.errors[:number].size).to eq(0)

        episode.number = 'test'
        expect(episode.valid?).to be_falsey
        expect(episode.errors[:number].size).to eq(1)
      end
    end
  end
end
