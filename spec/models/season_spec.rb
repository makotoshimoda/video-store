# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Season, type: :model do
  let(:season) { FactoryBot.build(:season) }

  describe 'data validations' do
    describe '.title' do
      it 'should be defined' do
        season.title = Faker::Lorem.characters(number: 10)
        expect(season.valid?).to be_truthy
        expect(season.errors[:title].size).to eq(0)

        season.title = nil
        expect(season.valid?).to be_falsey
        expect(season.errors[:title].size).to eq(1)
      end
    end

    describe '.plot' do
      it 'should be defined' do
        season.plot = Faker::Lorem.characters(number: 10)
        expect(season.valid?).to be_truthy
        expect(season.errors[:plot].size).to eq(0)

        season.plot = nil
        expect(season.valid?).to be_falsey
        expect(season.errors[:plot].size).to eq(1)
      end
    end
  end
end
