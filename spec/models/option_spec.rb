# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Option, type: :model do
  let(:option) { FactoryBot.build(:option) }

  describe 'data validations' do
    describe '.price' do
      it 'should be a number' do
        option.price = 10
        expect(option.valid?).to be_truthy
        expect(option.errors[:price].size).to eq(0)

        option.price = 'test'
        expect(option.valid?).to be_falsey
        expect(option.errors[:price].size).to eq(1)
      end
    end

    describe '.quality' do
      it 'must have an accepted value' do
        option.quality = 'HD'
        expect(option.valid?).to be_truthy
        expect(option.errors[:plot].size).to eq(0)

        option.quality = nil
        expect(option.valid?).to be_falsey
        expect(option.errors[:quality].size).to eq(1)
      end
    end
  end
end
