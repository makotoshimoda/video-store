# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Movie, type: :model do
  let(:movie) { FactoryBot.build(:movie) }

  describe 'data validations' do
    describe '.title' do
      it 'should be defined' do
        movie.title = Faker::Lorem.characters(number: 10)
        expect(movie.valid?).to be_truthy
        expect(movie.errors[:title].size).to eq(0)

        movie.title = nil
        expect(movie.valid?).to be_falsey
        expect(movie.errors[:title].size).to eq(1)
      end
    end

    describe '.plot' do
      it 'should be defined' do
        movie.plot = Faker::Lorem.characters(number: 10)
        expect(movie.valid?).to be_truthy
        expect(movie.errors[:plot].size).to eq(0)

        movie.plot = nil
        expect(movie.valid?).to be_falsey
        expect(movie.errors[:plot].size).to eq(1)
      end
    end
  end
end
