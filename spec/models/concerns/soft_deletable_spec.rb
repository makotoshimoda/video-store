# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SoftDeletable do
  describe '.soft_delete' do
    let(:purchase) { FactoryBot.create(:purchase) }

    it 'soft delete the record' do
      purchase.soft_delete
      expect(purchase.deleted_at).to_not eq(nil)
    end
  end
end
