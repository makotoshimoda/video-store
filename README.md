Video Store API
============

## Requirements

The following dependencies needs to be previously installed :

* Ruby (>= 2.5.6)
* Rails (>= 5.2.4)
* Postgres
* Redis

## Installation

1. Clone this repository on your local computer

	```
	git clone git@bitbucket.org:makotoshimoda/video-store.git
	```
	#

2. Copy `config/database.sample.yml` to `config/database.yml`

	```
	cp config/database.sample.yml config/database.yml
	```
	#

3. Copy `.env.sample` to `.env`

	```
	cp .env.sample .env
	```
	#

4. Install Rails dependencies

	```
	bundle
	```
	#

5. Create development and test databases

	```
	rails db:create
	```
	#

6. Create tables for both test and development databases

	```
	rails db:migrate && rails db:migrate RAILS_ENV=test
	```
	#

7. Invoke the `seeder`

	```
	bundle exec rake db:seed
	```
	#

## Available Endpoints

#### Movies
#
```http
GET http://localhost:3000/movies
```
#

#### Seasons
#
```http
GET http://localhost:3000/seasons
```
#

#### Products (Movies + Seasons)
#
```http
GET http://localhost:3000/products
```
#

#### Purchases
#
```http
GET http://localhost:3000/users/:user_id/purchases
```
#
#
```http
POST http://localhost:3000/users/:user_id/purchases
```
#
